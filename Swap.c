#include<stdio.h>
void swap(int*,int*);
int main()
{
    int a,b;
    printf("Please Enter the Value For a and b:\n");
    scanf("%d%d",&a,&b);
    printf("a=%d\n b=%d\n",a,b);
    swap(&a,&b);
     printf("After Interchanging-\n\n a=%d\n b=%d\n",a,b);
    return 0;
}
void swap(int* x,int* y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}